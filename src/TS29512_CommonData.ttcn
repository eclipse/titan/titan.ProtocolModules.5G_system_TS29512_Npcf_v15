/******************************************************************************
* Copyright (c) 2000-2023 Ericsson AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Gabor Szalai - initial implementation and initial documentation
******************************************************************************/
//
//  File:               TS29512_CommonData.ttcn
//  Description:	      Type definitions for 3GPP TS29512
/////////////////////////////////////////////// 
module TS29512_CommonData{
  import from TS29571_CommonData all;

  type set ChargingInformation {
    TS29571_CommonData.Uri    primaryChfAddress,
    TS29571_CommonData.Uri    secondaryChfAddress
  }
  type enumerated FlowDirection_enum { DOWNLINK, UPLINK, BIDIRECTIONAL, UNSPECIFIED}
 
  type union FlowDirection {
    FlowDirection_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }
 
} with {
  encode "JSON"
}

